---
title: Mid-Market Customer Advisory Group
description: Join our Customer Advisory Group
---

# Mid-Market Customer Advisory Group

After years of work with large enterprises, we're investigating how Database Lab
can help mid-size software companies (15-100 engineers) improve the safety and
testability of their database changes.

We're recruiting a small number of companies to join a Customer Advisory Group
to help us tailor our product to their needs.  Group members will receive free
access to Database Lab for 6 months.

## What's involved?

Companies who are accepted into the Customer Advisory Group will make the following commitments.

* They will fully setup the [Database Lab Engine](/products/how-it-works) and [Joe Bot](/products/joe).
* They will designate at least two people to serve as internal product champions for Database Lab.
* The designated product champions will attend a monthly, 30-minute feedback session with our team.

## Why join?

* Gain all the benefits of Database Lab, including
  [automated database migration testing](/products/database-migration-testing),
  [seamless SQL optimization](/products/joe),
  and [realistic test environments](/products/realistic-test-environments).
  Free for 6 months.
* Shape our product roadmap to the needs of your organization.
* _Added Bonus:_ We will run a free PostgreSQL Query Optimization Training course for your whole team based
  on the #1 voted talk from this year's [PGConf.Online](https://pgconf.ru/en/2021).

## Who is qualified?

* Company must have 15-100 software engineers
* Company must use PostgreSQL as its primary database
* Company must be willing to commit to a monthly 30-minute meeting to provide feedback


Questions? Contact us using the Intercom bubble to the right and leave an email so we can reach back out.


<div style={{'margin-top': '50px'}}>
  <a className="btn btn1" style={{'margin-right': '20px'}} href="https://forms.gle/X8LK5h9bwgYhSQPJ7">Apply Now!</a>
</div>
