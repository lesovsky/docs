---
title: Senior Full Stack Developer | React | Go
requirements:
  - 3+ years experience in client-side development using React
  - Solid skills of basic SQL (SQL-92)
  - Excellent HTML, CSS, JavaScript skills – you understand not only how to build the data, but how to make it look great too
---

# Senior Full Stack Developer | React | Go

As a Senior Full Stack Developer, you will be building components of the [Postgres.ai Platform](https://postgres.ai/docs).

## Job Details

- Employment type: full-time, remote
- Company: Postgres.ai, headquartered in California

## Requirements

- 3+ years experience in client-side development using React
- Solid skills of basic SQL (SQL-92)
- Excellent HTML, CSS, JavaScript skills – you understand not only how to build the data, but how to make it look great too
- Strong experience in all aspects of client-side performance optimization
- Deep understanding of HTTP protocol, data structures, JSON
- Rest API development experience

## Nice-to-haves

None of the following is a requirement, yet having any of these items increases your chances to be a perfect match for the Postgres.ai team.

- Experience in developing server applications using Go, or readiness to learn Go
- PostgreSQL experience is a big plus
- Advanced knowledge of CI/CD tools
- Contributions to Open Source projects
- Deep understanding of containerization concepts and tools, Docker specifically
- Kubernetes experience is a big plus
- Good command of English
- Solid knowledge of Computer Science fundamentals including the following topics:
   - Data Structures
   - Algorithms, and
   - System Optimization
- Experience in working remotely

## Benefits

- Development of game-changing tools for software engineers
- Interesting and challenging tasks, basis for constant learning of new technologies
- Team of professionals and a supportive atmosphere
- Extremely competitive pay depending on experience and skills
- Flexible working hours/home-office
- Help with relocation

Send us your CV to join@postgres.ai
