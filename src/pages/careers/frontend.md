---
title: Senior Frontend Developer
requirements:
  - 3+ years experience in client-side development using React
  - Excellent HTML, CSS, JavaScript skills – you understand not only how to build the data, but how to make it look great too
  - Strong experience in all aspects of client-side performance optimization
---

# Senior Frontend Developer

As a Senior Frontend Developer, you will be building components of the [Postgres.ai Platform](https://postgres.ai/docs).

## Job Details

- Employment type: full-time/ part-time remote
- Company: Postgres.ai, headquartered in California

## Requirements

- 3+ years experience in client-side development using React
- Excellent HTML, CSS, JavaScript skills – you understand not only how to build the data, but how to make it look great too
- Strong experience in all aspects of client-side performance optimization
- Deep understanding of HTTP protocol, data structures, JSON
- Technical English
- Experience in working remotely


## Nice-to-haves

None of the following is a requirement, yet having any of these items increases your chances to be a perfect match for the Postgres.ai team.

- Rest API development experience
- PostgreSQL experience is a plus
- Advanced knowledge of CI/CD tools
- Contributions to Open Source projects
- Understanding of containerization concepts and tools, Docker specifically
- Good command of English
- Solid knowledge of Computer Science fundamentals including the following topics:
   - Data Structures
   - Algorithms, and
   - System Optimization

## Benefits

- Development of game-changing tools for software engineers
- Interesting and challenging tasks, basis for constant learning of new technologies
- Team of professionals and a supportive atmosphere
- Extremely competitive pay depending on experience and skills
- Flexible working hours/home-office
- Help with relocation

Send us your CV to join@postgres.ai

