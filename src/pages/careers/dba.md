---
title: Senior Database Engineer
requirements:
  - 3+ years of experience in running PostgreSQL in large production environments
  - Strong skills in performance optimization of large and heavily loaded databases (>1TiB, >10k TPS)
  - Readiness to dive deep into PostgreSQL internals
---

# Senior Database Engineer

As a Senior Database Engineer, you will be working on state-of-the-art solutions, involving the following topics:

- Сapacity planning
- Database experiments
- Incident troubleshooting
- Database and CI/CD
- Cloud infrastructures
- Partitioning
- Sharding
- Database change management
- Performance optimization

## Requirements

- 3+ years of experience in running PostgreSQL in large production environments
- Strong skills in performance optimization of large and heavily loaded databases (>1TiB, >10k TPS)
- Readiness to dive deep into PostgreSQL internals
- A proven track record of increasing responsibility in the field of databases
- Experience and skills in the field of systems performance (PostgreSQL and Linux monitoring, troubleshooting, tuning)
- Self-motivation and strong desire to achieve the highest levels of expertise
- Solid skills of reading and using EXPLAIN command to troubleshoot and optimize SQL queries
- Technical English
- Advanced SQL and PL/pgSQL knowledge

## Nice-to-haves

- Experience in partitioning
- Experience in sharding
- Experience in implementation of SQL optimization workflow, involving `pg_stat_statements`, log-based analysis, etc.
- Experience in development/tuning of advanced PostgreSQL monitoring setups either from scratch or based on existing components
- Deep understanding of file systems
- ZFS experience
- Go and/or C
- Python and/or Ruby
- Oracle or SQL Server scalability and performance optimization experience
- Knowledge of basic machine learning algorithms and experience/with popular machine learning frameworks
- Cloud experience (AWS, GCP)
- Experience in working with logical decoding and replication
- Experience in working in a distributed team
- Good verbal/written skills in English

## Responsibilities

- Development of new open-source to automate:
  - SQL query optimization
  - Database experiments to verify DB migrations, complex changes, etc
  - PostgreSQL configuration tuning
- SQL performance troubleshooting and optimization
- DB schema design to store data securely and efficiently
- Code reviews and interaction with development teams
- Setting up SQL optimization workflow in various development teams
- Direct work with our clients to help them scale and optimize their PostgreSQL databases
- Assisting the development team to develop brand new tools that solve database scalability and performance problems

## Benefits

- Development of game-changing tools for software engineers
- Interesting and challenging tasks, basis for constant learning of new technologies
- Team of professionals and a supportive atmosphere
- Extremely competitive pay depending on experience and skills
- Flexible working hours/home-office
- Help with relocation

Send us your CV to join@postgres.ai
