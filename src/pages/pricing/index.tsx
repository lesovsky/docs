import React from 'react'
import Layout from '@theme/Layout'
import useDocusaurusContext from '@docusaurus/useDocusaurusContext'

import styles from './styles.module.css'

const Pricing = () => {
  const { siteConfig } = useDocusaurusContext()
  const { customFields } = siteConfig
  const { signInUrl } = customFields

  return (
    <Layout>
      <section className={styles.title}>
        <h1 className="em">Pricing that scales with you</h1>
      </section>

      <div className="container text-center">
        <a className="supportOffer" href="mailto: sales@postgres.ai">
          Do you need only reliable support? We offer Support SLA without
          advanced Database Lab Platform plans. Contact us for more details.
        </a>
      </div>

      <section className="pricing-table position-relative">
        <div className="container text-center">
          <div className="row price-row">
            <div className="col-md-2">&nbsp;</div>
            <div className="col-md-3 price-box">
              <h3>Community Edition</h3>
              <div className="em">Open Source</div>
              <a
                className="btn btn2 cta"
                href="https://gitlab.com/postgres-ai/database-lab"
                target="_blank"
              >
                View repo
              </a>
              <ul className="mobile-feature-list">
                <li>Unlimited Thin Cloning</li>
              </ul>
            </div>
            <div className="col-md-3 price-box">
              <h3>Standard</h3>
              <div className="em">$190 per month</div>
              <div>
                for each 100 GiB of data<sup>1</sup>
              </div>
              <a className="btn btn1 cta" href="https://postgres.ai/console/">
                Start free trial
              </a>
              <ul className="mobile-feature-list">
                <li>Unlimited Thin Cloning</li>
                <li>SQL Optimization UI</li>
                <li>Security & User Management Features</li>
                <li>Business hour support with 24 hour response time</li>
              </ul>
            </div>
            <div className="col-md-3 price-box">
              <h3>Enterprise</h3>
              <div className="em">Custom Pricing</div>
              <div>annual contracts</div>
              <a className="btn btn1 cta" href="mailto: sales@postgres.ai">
                Contact us
              </a>
              <ul className="mobile-feature-list">
                <li>Unlimited Thin Cloning</li>
                <li>SQL Optimization UI</li>
                <li>Advanced User Management & SSO</li>
                <li>24 / 7 / 365 support with 1 hour response time</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="container text-center pricing-feature-table">
          <div className={`row ${styles.featureRow}`}>
            <div className="col-md-2 feature-name">Thin Cloning</div>
            <div className="col-md-3">Unlimited</div>
            <div className="col-md-3">Unlimited</div>
            <div className="col-md-3">Unlimited</div>
          </div>
          <div className={`row ${styles.featureRow}`}>
            <div className="col-md-2 feature-name">Cloning UI</div>
            <div className="col-md-3">Included</div>
            <div className="col-md-3">Included</div>
            <div className="col-md-3">Included</div>
          </div>
          <div className={`row ${styles.featureRow}`}>
            <div className="col-md-2 feature-name">SQL Optimization UI</div>
            <div className="col-md-3">
              <i>Not Included</i>
            </div>
            <div className="col-md-3">
              <ul>
                <li>Joe Bot secure web interface</li>
                <li>
                  <code>EXPLAIN ANALYZE</code> visualization
                </li>
              </ul>
            </div>
            <div className="col-md-3">
              <i>All Standard Features</i>
            </div>
          </div>
          <div className={`row ${styles.featureRow}`}>
            <div className="col-md-2 feature-name">
              Security & User Management
            </div>
            <div className="col-md-3">
              <i>Not Included</i>
            </div>
            <div className="col-md-3">
              <ul>
                <li>Secure User Access Tokens</li>
                <li>Basic Access Control</li>
                <li>Audit Log</li>
              </ul>
            </div>
            <div className="col-md-3">
              <i>All Standard Features</i>+<br />
              <ul>
                <li>Advanced Access Control</li>
                <li>Enterprise SSO</li>
              </ul>
            </div>
          </div>
          <div className={`row ${styles.featureRow}`}>
            <div className="col-md-2 feature-name">Support SLA</div>
            <div className="col-md-3">
              <i>Not Included</i>
            </div>
            <div className="col-md-3">
              <div>Monday - Friday</div>
              <div>24 hour response time</div>
            </div>
            <div className="col-md-3">
              <div>24 / 7 / 365</div>
              <div>1 hour response time</div>
            </div>
          </div>
        </div>
      </section>

      <section className="pricing-details position-relative">
        <div className="container">
          <div className="row">
            <div className="col-md-9 offset-2">
              <ul className="footnotes">
                <li>
                  <sup>1</sup> Service is priced per GiB / hour at a rate of
                  $0.0026. Database Lab monitors the physical size of the
                  database using <code>df</code>&nbsp; on an hourly basis.
                  Standard and Enterprise packages have access to the Database
                  Lab Platform.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Pricing
